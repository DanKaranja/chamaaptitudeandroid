package cyclone.chamaaptitude;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;

public class SolutionActivity extends AppCompatActivity {


    private Queue<Double> PrizeValues = new LinkedList<>();
    private Map<String,List<Double>> Winners = new HashMap<>();
    private EditText _ediText_winners;
    private EditText _ediText_Prizes;
    private Button _btn_process;
    private Button _btn_reset;
    private RecyclerView _recy_winnings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solution);/*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        _ediText_winners = (EditText) findViewById(R.id.ediText_winners);
        _ediText_Prizes = (EditText) findViewById(R.id.ediText_Prizes);
        _btn_process = (Button) findViewById(R.id.btn_process);
        _btn_reset = (Button) findViewById(R.id.btn_reset);
        _recy_winnings = (RecyclerView) findViewById(R.id.recy_winnings);

        _btn_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    hideSoftKeyboard();
                    GFPrizes(inputToStringList(_ediText_Prizes.getText().toString()));
                    GFWinners(inputToStringList(_ediText_winners.getText().toString()));
                    process();
                    PrizeValues.clear();
                    Winners.clear();
                } catch (Exception e) {
                    Toast.makeText(getBaseContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });
        _btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
                clear();
            }
        });
    }
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
    private void clear(){
        _ediText_Prizes.setText("");
        _ediText_winners.setText("");
        PrizeValues.clear();
        Winners.clear();
        SetupRecyclerView(new ArrayList<String[]>());
    }

    private void process(){
        //if K is not equal to n
        ArrayList<String[]> output = new ArrayList<>();
        if(Winners.size() != PrizeValues.size()) {
            //Distribute the first K prizes between the Winners
            //Note: The n prizes are sorted in descending value
            for(String winner : Winners.keySet()){
                Winners.get(winner).add(PrizeValues.poll());
            }

            //Distribute the remaining n-k prizes between the lowest earning winners as it changes
            while (!PrizeValues.isEmpty()) {
                Winners.get(findLeastWinner()).add(PrizeValues.poll());
            }
            //Display results
            for(String winner : Winners.keySet()){
                output.add(new String[]{winner,Winners.get(winner).toString(),String.valueOf(getSum(Winners.get(winner)))});
            }
        }else{
            //K is equal to n
            List<Double> valueList = new ArrayList<>();
            valueList.addAll(PrizeValues);
            //Randomly distribute the prizes between the winners
            for(String winner : Winners.keySet()){
                int chance = (int) (Math.random() * valueList.size() + 0);
                output.add(new String[]{winner,String.valueOf(valueList.get(chance)),String.valueOf(valueList.get(chance))});
                valueList.remove(chance);
            }

        }
        SetupRecyclerView(output);
    }
    private void SetupRecyclerView(ArrayList<String[]> output) {
        _recy_winnings.setLayoutManager(new LinearLayoutManager(this));
        _recy_winnings.setAdapter(new WinningsRecyclerAdapter(output,this));
        _recy_winnings.setHasFixedSize(true);
    }


    private String findLeastWinner(){
        //Return the winner with the current lowest winnings
        Double curMin = Double.MAX_VALUE;
        String leastWinner = null;
        for(String winner : Winners.keySet()){
            Double curSum = getSum(Winners.get(winner));
            if(curSum < curMin){
                curMin = curSum;
                leastWinner = winner;
            }
        }
        return leastWinner;
    }

    private Double getSum(List<Double> data){
        Double sum = 0.0;
        for(Double stuff : data){
            sum += stuff;
        }
        return sum;
    }

    private List<String> inputToStringList(String input) throws Exception {
        //Returns a list of the string delimited by ','
        //Breaks if input string is empty or has terminating char : '!'
        if((!input.equals("!"))&&(!input.isEmpty())) {
            StringTokenizer tokenizer = new StringTokenizer(input, ",");
            List<String> outputList = new ArrayList<>(tokenizer.countTokens());
            while (tokenizer.hasMoreTokens()) {
                outputList.add(tokenizer.nextToken());
            }
            return outputList;
        }else
            throw new Exception("Program terminated");
    }
    private void GFWinners(List<String> raw) throws Exception{
        for(String string : raw){
            string.trim();
            Winners.put(string,new ArrayList<Double>());
        }
    }

    private void GFPrizes(List<String> raw) throws Exception {
        List<Double> prizeValues = new ArrayList<>();
        for(String string : raw){
            Double value = Double.valueOf(string);
            if(value > 0 )
                prizeValues.add(value);
            else
                throw new Exception("Cannot have negative winnings");
        }
        Collections.sort(prizeValues, new Comparator<Double>() {
            @Override
            public int compare(Double o1, Double o2) {
                return o2.compareTo(o1);
            }
        });
        PrizeValues.addAll(prizeValues);

    }


    public class WinningsRecyclerAdapter extends RecyclerView.Adapter<WinningsRecyclerAdapter.ViewHolder>
    {
        Activity activity;
        private ArrayList<String[]> mItems;

        public WinningsRecyclerAdapter(ArrayList<String[]> winnings,Activity activity) {
            super();
            this.mItems = winnings;
            this.activity = activity;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_winnings, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final String[] winning = mItems.get(position);
            holder._winnings_winner.setText(activity.getResources().getString(R.string.recy_prizes_Winner,winning[0]));
            holder._winnings_prizes.setText(activity.getResources().getString(R.string.recy_prizes_Winnings,winning[1]));
            holder._winning_value.setText(activity.getResources().getString(R.string.recy_prizes_Value,winning[2]));
        }


        @Override
        public int getItemCount() {
            return mItems.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{

            public TextView _winnings_winner;
            public TextView _winnings_prizes;
            public TextView _winning_value;

            public ViewHolder(View itemView) {
                super(itemView);
                _winnings_winner = (TextView) itemView.findViewById(R.id.winnings_winner);
                _winnings_prizes = (TextView) itemView.findViewById(R.id.winnings_prizes);
                _winning_value = (TextView) itemView.findViewById(R.id.winning_value);
            }
        }
    }



}
