# Chamasoft Aptitude Coding challenge
=======================================

### Please provide:

* Solution in Java, with no external libraries used except for test purposes (libraries such as JUnit are acceptable).
* A brief text description of your definition of 'fair' as applied in your code.


### THE UNFAIR LOTTERY 
==================

A lottery company shares out prizes to winning contestants every week. Most weeks, more than one contestant wins, in which case they try to share out the prizes as fairly as possible. Their prize distribution office has hired you to write a program that they will use to distribute prizes in the fairest way possible.

The program you write should take two lines of input:

* A comma-separated list of this week's prizes' values_
* A comma-separated names of this week's winners_

For example, the input could be:

> 100,800,200,500,400,1000

> Joshua,Mahesh,Lilian

The program should then output the fairest possible distribution of prizes, by displaying one line for each winner, with the values of the prizes allocated to them. For example, given the input above, the output could be:

> Joshua:100,400,500

> Mahesh:1000

> Lilian:800,200

The example above gives a perfect solution, where all winners get the same value of prizes (total value of 1000 each). In many cases, this will not be possible, but all prizes must be distributed and cannot be divided. Part of your job is to decide how you define 'fair' for these cases. For example, given the input

> 400,400,500,600

> Barry,Sheila,Onyango,Wekesa

The following would be acceptable output, because there is no fairer distribution possible:

> Barry:400

> Sheila:400

> Onyango:500

> Wekesa:600

## FAIRNESS REASONING 
=====================

+ If the number of winners and prizes are the same, then the prizes are randomly distributed amongst the winners 
+ If not, then a fairness algorithm is employed. The algorithm firstly distributes the heaviest weighted prizes amongst all the winners. It then determines the lowest winner ( a winner with the lowest total value of winnings) and assigns them the largest valued price in the remaining prizes. A new lowest winner is recalculated again and assigned a prize, this is repeated untill no prizes are left to distribute.

## JAVA SOLUTION
==================

```java
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {
    static Scanner scanner = new Scanner(System. in);
    static Queue<Double> PrizeValues = new LinkedList<>();
    static Map<String,List<Double>> Winners = new HashMap<>();

    public static void main(String[] args) {
        //Let K be the number of winners
        //let n be the number of prizes
        //Driver
        while(true){
            System.out.println("|||||||START|||||||");
            System.out.println();
            //Input prize values
            // '!' is the terminating char
            try {
                //extract prize values in double
                System.out.println("Enter comma-separated line of prize values : ");
                GFPrizes(inputToStringList(scanner.nextLine()));

                //Request of winner strings
                System.out.println("Enter comma-separated line of winners : ");
                Winners = inputToStringList(scanner.nextLine()).stream().collect(Collectors.toMap(Function.identity(),w -> new ArrayList<Double>()));

                //Process the winnings
                process();
                PrizeValues.clear();
                Winners.clear();
            }catch (Exception e){
                e.printStackTrace();
                break;
            }
        }

    }
    public static void process(){
        System.out.println();
        System.out.println("-----RESULTS-----");
        System.out.println();
        //if K is not equal to n
        if(Winners.size() != PrizeValues.size()) {
            //Distribute the first K prizes between the Winners
            //Note: The n prizes are sorted in descending value
            Winners.keySet().forEach(winner -> Winners.get(winner).add(PrizeValues.poll()));

            //Distribute the remaining n-k prizes between the lowest earning winners as it changes
            while (!PrizeValues.isEmpty()) {
                Winners.get(findLeastWinner()).add(PrizeValues.poll());
            }
            //Display results
            Winners.keySet().stream().forEach(winner -> System.out.println(winner + " : " + Winners.get(winner).toString()+" -> "+Winners.get(winner).stream().reduce(0.0,Double::sum)));
        }else{
            //K is equal to n
            List<Double> valueList = new ArrayList<>();
            valueList.addAll(PrizeValues);
            //Randomly distribute the prizes between the winners
            Winners.keySet().stream().forEach(w -> {
                int chance = (int) (Math.random() * valueList.size() + 0);
                System.out.println(w + " : " + valueList.get(chance));
                valueList.remove(chance);
                    });
        }

        System.out.println();
        System.out.println("-----END-----");
        System.out.println();
    }
    public static String findLeastWinner(){
        //Return the winner with the current lowest winnings
        Double curMin = Double.MAX_VALUE;
        String leastWinner = null;
        for(String winner : Winners.keySet()){
            Double curSum = Winners.get(winner).stream().reduce(0.0, Double::sum);
            if(curSum < curMin){
                curMin = curSum;
                leastWinner = winner;
            }
        }
        return leastWinner;
    }

    public static List<String> inputToStringList(String input) throws Exception {
        //Returns a list of the string delimited by ','
        //Breaks if input string is empty or has terminating char : '!'
        if((!input.equals("!"))&&(!input.isEmpty())) {
            StringTokenizer tokenizer = new StringTokenizer(input, ",");
            List<String> outputList = new ArrayList<>(tokenizer.countTokens());
            while (tokenizer.hasMoreTokens()) {
                outputList.add(tokenizer.nextToken());
            }
            return outputList;
        }else
            throw new Exception("Program terminated");
    }

    public static void GFPrizes(List<String> raw) throws Exception {
        List<Double> prizeValues = new ArrayList<>();
        for(String string : raw){
            Double value = Double.valueOf(string);
            if(value > 0 )
                prizeValues.add(value);
            else
                throw new Exception("Cannot have negative winnings");
        }
        Collections.sort(prizeValues, (o1, o2) -> o2.compareTo(o1));
        PrizeValues.addAll(prizeValues);

    }


}
```